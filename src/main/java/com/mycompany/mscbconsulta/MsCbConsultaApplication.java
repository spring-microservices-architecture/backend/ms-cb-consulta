package com.mycompany.mscbconsulta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class MsCbConsultaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsCbConsultaApplication.class, args);
	}

}
