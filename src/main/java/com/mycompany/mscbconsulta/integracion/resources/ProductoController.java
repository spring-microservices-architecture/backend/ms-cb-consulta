package com.mycompany.mscbconsulta.integracion.resources;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.mscbconsulta.integracion.dto.ProductoReadDto;


@RestController
@RequestMapping("/productos")
public class ProductoController {

	private List<ProductoReadDto> listProducto;
	
	@PostConstruct
	public void loadProducts() {
		listProducto = new ArrayList<>();
		listProducto.add(new ProductoReadDto(1L, "AGUA", 9));
		listProducto.add(new ProductoReadDto(2L, "ACEITE", 12));
		listProducto.add(new ProductoReadDto(3L, "CAMOTE", 5));
		listProducto.add(new ProductoReadDto(4L, "AGUACATE", 7));
		listProducto.add(new ProductoReadDto(5L, "ACEITUNA", 3));
	}

	@GetMapping
	public ResponseEntity<List<ProductoReadDto>> getAll() {
		return ResponseEntity.ok().body(listProducto);
	}

	@GetMapping("/{codigo}")
	public ResponseEntity<ProductoReadDto> getByCodigo(
			@PathVariable(value = "codigo")
			Long codigo) {
		return ResponseEntity.ok().body(
				listProducto.stream()
				.filter(prod -> prod.getCodigo().equals(codigo))
				.findFirst().orElse(new ProductoReadDto()));
	}

	@GetMapping("/descripcion/{descripcion}")
	public ResponseEntity<List<ProductoReadDto>> getByDescripcion(
			@PathVariable(value = "descripcion")
			String descripcion) {
		return ResponseEntity.ok().body(
				listProducto.stream()
				.filter(prod -> prod.getDescripcion().contains(descripcion))
				.collect(Collectors.toList()));
	}
	
	public List<ProductoReadDto> getListProducto() {
		return listProducto;
	}

	public void setListProducto(List<ProductoReadDto> listProducto) {
		this.listProducto = listProducto;
	}
}